Rails.application.routes.draw do
  get 'page/checkout' 
  get 'page/confirmation'
  get 'page/order'

  resources :carts
  resources :line_items
  resources :products
  resources :charges
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'
  
 get 'account' => 'account#index'
 get "/account/:page" => "account#show"

end
