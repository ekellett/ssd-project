require 'test_helper'

class PageControllerTest < ActionController::TestCase
  test "should get checkout" do
    get :checkout
    assert_response :success
  end

  test "should get confirmation" do
    get :confirmation
    assert_response :success
  end

  test "should get order" do
    get :order
    assert_response :success
  end

end
