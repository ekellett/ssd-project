CREATE TABLE "schema_migrations" ("version" varchar NOT NULL);
CREATE UNIQUE INDEX "unique_schema_migrations" ON "schema_migrations" ("version");
CREATE TABLE "categories" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "users" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "email" varchar DEFAULT '' NOT NULL, "encrypted_password" varchar DEFAULT '' NOT NULL, "reset_password_token" varchar, "reset_password_sent_at" datetime, "remember_created_at" datetime, "sign_in_count" integer DEFAULT 0 NOT NULL, "current_sign_in_at" datetime, "last_sign_in_at" datetime, "current_sign_in_ip" varchar, "last_sign_in_ip" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, "address" varchar, "phone" varchar, "lastname" varchar, "firstname" varchar, "admin" boolean DEFAULT 'f', "membership" boolean);
CREATE UNIQUE INDEX "index_users_on_email" ON "users" ("email");
CREATE UNIQUE INDEX "index_users_on_reset_password_token" ON "users" ("reset_password_token");
CREATE TABLE "products" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "length" varchar, "description" varchar, "price" float, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, "category_id" integer);
CREATE TABLE "carts" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, "user_id" integer);
CREATE TABLE "line_items" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "product_id" integer, "cart_id" integer, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, "quantity" integer DEFAULT 1);
INSERT INTO schema_migrations (version) VALUES ('20160712172628');

INSERT INTO schema_migrations (version) VALUES ('20160712180310');

INSERT INTO schema_migrations (version) VALUES ('20160713103817');

INSERT INTO schema_migrations (version) VALUES ('20160713104153');

INSERT INTO schema_migrations (version) VALUES ('20160713173956');

INSERT INTO schema_migrations (version) VALUES ('20160713174134');

INSERT INTO schema_migrations (version) VALUES ('20160713174154');

INSERT INTO schema_migrations (version) VALUES ('20160713185210');

INSERT INTO schema_migrations (version) VALUES ('20160713185757');

INSERT INTO schema_migrations (version) VALUES ('20160713190048');

INSERT INTO schema_migrations (version) VALUES ('20160715121749');

INSERT INTO schema_migrations (version) VALUES ('20160715155706');

INSERT INTO schema_migrations (version) VALUES ('20160715161927');

INSERT INTO schema_migrations (version) VALUES ('20160720122912');

INSERT INTO schema_migrations (version) VALUES ('20160723161454');

INSERT INTO schema_migrations (version) VALUES ('20160723161945');

INSERT INTO schema_migrations (version) VALUES ('20160723165334');

INSERT INTO schema_migrations (version) VALUES ('20160723211529');

