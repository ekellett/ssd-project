# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Product.create(:name => 'Holistic Full Body Massage', :length => '60 mins', :description => 'Holistic Massage is adapted to the needs of the modern stressful life. Its aim is to treat the whole person body mind and soul. Holistic Massage relaxes the muscles, calms the emotions, improves the circulation, recharges the body’s immune system, refreshes the spirit and balances the life forces.', :price => '85.00')
Product.create(:name => 'Deep Tissue Massage', :length => '60 mins', :description => 'Deep tissue massage is a style of massage that is designed to get into the connective tissue of the body and work on the deeper muscle structures, rather than just the surface muscles. It is highly stimulating. Ideal for jetlag, pre or post sports activity or simply after a hectic week.', :price => '85.00')
Product.create(:name => 'Sports Massage', :length => '60 mins', :description => 'A massage technique for athletes aimed at either preparing the muscle and joints for athletic activity or to help recover from stress and strain associated with exercise.', :price => '90.00')
Product.create(:name => 'Swedish Massage', :length => '60 mins', :description => 'This is the classical European massage. The term Swedish Massage refers to a variety of techniques specifically designed to relax muscles by applying pressure to them against deeper muscles and bones, and rubbing in the same direction as the flow of blood returning to the heart. Stimulates circulation, eases muscle tension, improves flexibility, promotes relaxation and is highly invigorating.', :price => '90.00')

Category.create(:name => 'Massage')
Category.create(:name => 'Reflexology')
Category.create(:name => 'Manicure')
Category.create(:name => 'Pedicure')
Category.create(:name => 'Facials')
Category.create(:name => 'Aromatherapy Treatments')
Category.create(:name => 'Special Offers')
