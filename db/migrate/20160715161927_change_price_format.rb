class ChangePriceFormat < ActiveRecord::Migration
  def change
    change_column :products , :price, :float, :precision => 8, :scale => 2
  end
end
