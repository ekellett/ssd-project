class AccountController < ApplicationController
    
    def index
    end
    
    def show
        # Adding page for membership http://blog.teamtreehouse.com/static-pages-ruby-rails
        render template: "account/#{params[:page]}"
    end
    
end
