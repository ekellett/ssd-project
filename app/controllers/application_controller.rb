class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
   before_action :configure_permitted_parameters, if: :devise_controller?
   before_filter :set_total
   
    def set_total
        @cart = current_cart
    end
    
   protected

  def configure_permitted_parameters
     devise_parameter_sanitizer.for(:sign_up) << :firstname << :lastname << :address << :phone << :membership
     devise_parameter_sanitizer.for(:account_update) << :firstname << :lastname << :address << :phone << :membership
  end
  
  private
    def current_cart
        #finds the current cart
       Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
        #if none exists a cart is created
        cart = Cart.create
        session[:cart_id] = cart.id
        cart
    end
    
    
end
