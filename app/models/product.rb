class Product < ActiveRecord::Base
    has_many :line_items
    belongs_to :category
    
    def self.search(search)
        where("name LIKE ?", "%#{search}%") 
        where("description LIKE ?", "%#{search}%")
    end

end
