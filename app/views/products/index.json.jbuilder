json.array!(@products) do |product|
  json.extract! product, :id, :name, :length, :description, :price
  json.url product_url(product, format: :json)
end
